import './style.scss'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'
import { gsap } from 'gsap'
import {ScrollTrigger} from 'gsap/ScrollTrigger'
import { TextPlugin } from "gsap/TextPlugin"
import { Observer } from "gsap/Observer";
import { Draggable } from "gsap/Draggable";
import { Flip } from "gsap/Flip";

gsap.registerPlugin(ScrollTrigger, Observer, Draggable, TextPlugin, Flip);

/********* replace img & text on scroll animation ************/
gsap.set('.main', {position:'fixed', background:'#fff', width:'100%', maxWidth:'1200px', height:'100%', top:0, left:'50%', x:'-50%'})
gsap.set('.scrollDist', {width:'100%', height:'200%'})
gsap.timeline({scrollTrigger:{trigger:'.scrollDist', start:'top top', end:'bottom bottom', scrub:1}})
    .fromTo('.sky', {y:0},{y:-200}, 0)
    .fromTo('.cloud1', {y:100},{y:-800}, 0)
    .fromTo('.cloud2', {y:-150},{y:-500}, 0)
    .fromTo('.cloud3', {y:-50},{y:-650}, 0)
    .fromTo('.mountBg', {y:-10},{y:-100}, 0)
    .fromTo('.mountMg', {y:-30},{y:-250}, 0)
    .fromTo('.mountFg', {y:-50},{y:-600}, 0)

/******* svg animation scroll down *******/
let drop = gsap.to("#hero", {
  y: 8000,
  ease: "none",
  duration: 3
});

ScrollTrigger.create({
  trigger: "#scene",
  animation:drop,
  start: "top top",
  end: "bottom bottom",
  scrub: true,
  ease: "slow",
  markers: true
})

//replaces word-by-word because the delimiter is " " (a space)
gsap.to("h1", {
  x: 50,
  y: 50, 
  duration: 5, 
  text: {
    value: "", 
  }, 
  ease: "none"
});

/**************************************
 **************************************
 **************************************
 *****   custom custor animated   *****
 **************************************
 **************************************
 **************************************
 **************************************/
gsap.set('.follower', {
  xPercent: -50,
  yPercent: -50
});
gsap.set('.cursor', {
  xPercent: -50,
  yPercent: -50,
  ease: "power2.out"
});



const follow = document.querySelector('.follower');
const curs = document.querySelector('.cursor');

addEventListener('mousemove', (e) => {
  gsap.to(curs, 0.2, {
    x: e.clientX,
    y: e.clientY
  });
  gsap.to(follow, 0.9, {
    x: e.clientX,
    y: e.clientY
  });
})

/**** or custom cursor as ... : ****/
gsap.set(".ball", {
  xPercent: -50, 
  yPercent: -50
});
const ball = document.querySelector(".ball");
const pos = {
  x: window.innerHeight / 2,
  y: window.innerHeight / 2
};
const mouse = {
  x: pos.x,
  y: pos.y
};
const speed = 0.2;

const xSet = gsap.quickSetter(ball, "x", "px");
const ySet = gsap.quickSetter(ball, "y", "px");

window.addEventListener("mousemove", e => {
  mouse.x = e.x;
  mouse.y = e.y;
});

gsap.ticker.add(() => {
  const dt = 1.0 - Math.pow(1.0 - speed, 
    gsap.ticker.deltaRatio());
  
  pos.x += (mouse.x - pos.x) * dt;
  pos.y += (mouse.y - pos.y) * dt;
  xSet(pos.x);
  ySet(pos.y);

});

/*************************************************************************
**************************************************************************
*************************** Next animation *******************************
**************************************************************************
**************************************************************************/

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello Vite!</h1>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
    <p class="read-the-docs">
      Click on the Vite logo to learn more
    </p>
  </div>
`

setupCounter(document.querySelector('#counter'))
