# Install this vanilla javascript application

## Install app 

+ clone app repository, run : 

    `git clone "app-repo-name"`

+ go on the cloning repo :

    - `cd ...app-name...`

+ install dependencies, inclues the `node_modules` folder :

    - `yarn` or `npm init`

+ run local app, on `localhost`:
    
    - `yarn dev` or `npm dev`

+ Create with `vite.js` bundle, as cmd:

    `yarn create vite`

## Dependencies add

+ `GSAP`:

    - `yarn add gsap`

+ `GSAP`'s plugins, use `ScrollTrigger`, as:

```js

```

## Versionning on GitLab, with git

+ new project, run `git` cmd :

```
git init --initial-branch=main
git remote add origin git@gitlab.com:HuguesGouttebroze/affaire-a-suivre.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

